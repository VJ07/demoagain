package crud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
//@RestController
public class CrudApplication {

//    @Value("${shedlock.para}")
//    private List<String> message;
    public static void main(String[] args) {
        SpringApplication.run(CrudApplication.class,args);
        //log.info("Simple log statement with inputs {}, {} and {}", 1, 2, 3);
    }
//
//    @RequestMapping("hello")
//    public List<String> m()
//    {
//        List<String> stringList=message.stream().map(s->s.contains("0")? null:s).collect(Collectors.toList());
//        //List<String> stringList=message;
//        return stringList;
//    }
}
