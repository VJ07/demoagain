package crud.service;

import crud.entity.Address;
import crud.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IAddressService {

    // Save operation
    Address saveAddress(Address address);

    // Read operation
    List<Address> getAllAddress();

    // Update operation
    Address updateAddress(Address address);

    // Delete operation
    void deleteAddressById(Address address);

    //Read By Id
    Address getAddressById(int id);
}

