package crud.service;

import crud.entity.Employee;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IEmployeeService {

    // Save operation
    Employee saveEmployee(Employee employee);

    // Read operation
    List<Employee> getAllEmployee();

    // Update operation
    Employee updateEmployee(Employee employee);

    // Delete operation
    void deleteEmployeeById(Employee employee);

    //Read By Id
    Employee getEmployeeById(int id);

}
