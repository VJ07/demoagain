package crud.service;

import crud.dao.EmployeeRespository;
import crud.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService{

    @Autowired
    private EmployeeRespository employeeRespository;

    public EmployeeService(EmployeeRespository employeeRespository) {
        super();
        this.employeeRespository = employeeRespository;
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRespository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRespository.save(employee);
    }

    @Override
    public Employee getEmployeeById(int id) {
        return employeeRespository.findById(id).get();
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        return employeeRespository.save(employee);
    }

    @Override
    public void deleteEmployeeById(Employee employee) {
        employeeRespository.deleteById(employee.getEmp_Id());
    }
}
