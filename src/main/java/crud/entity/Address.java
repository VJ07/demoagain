package crud.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name="Address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ADDR_ID")
    private int addr_Id;

    @Column(name="ADDR_CITY")
    private String addr_City;

    @Column(name="ADDR_STATE")
    private String addr_State;

    @Column(name="ADDR_COUNTRY")
    private String addr_Country;

    @Column(name="ADDR_PERMANENT")
    private String addr_Permanent;
}
