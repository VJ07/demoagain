package crud.entity;

/*@Entity - This annotation specifies that the class is an entity. This annotation can be applied to Class, Interface of Enums.
@Table  - JPA annotation specifies the table in the database with which this entity is mapped.
@Id - The @Id JPA annotation specifies the primary key of the entity.
@Column - The @Column annotation is used to specify the mapping between a basic entity attribute and the database table column

 @Data generates all the boilerplate that is normally associated with simple POJOs (Plain Old Java Objects) and beans:

getter methods for all fields,
setter methods for all non-final fields,
 appropriate toString(),
appropriate equals() and
hashCode() implementations that involve the fields of the class,
and a constructor that initializes all final fields,
as well as all non-final fields with no initializer that have been marked with @NonNull, in order to ensure the field is never null.
*/

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/*https://javabydeveloper.com/lombok-data-annotation/*/

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="Employee")
public class Employee implements Serializable {

            @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Column(name="EMP_ID")
            private int emp_Id;
            @Column(name="EMP_FNAME")
            private String emp_Fname;
            @Column(name="EMP_LNAME")
            private String emp_Lname;
            @Column(name="EMP_PHONE")
            private String emp_Phone;

            /* these two are corrects i.e one is comment and another is uncomments */

//    @JoinColumns({
//            @JoinColumn(name = "EMP_ID", referencedColumnName = "ADDR_ID", insertable = false, updatable = false) })
//    @OneToOne(optional = false, fetch = FetchType.LAZY)
//    private Address address;

        @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
        @PrimaryKeyJoinColumn
        private Address address;

}
