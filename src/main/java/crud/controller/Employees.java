package crud.controller;

import crud.dao.EmployeeRespository;
import crud.entity.Employee;
import crud.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class Employees {

    @Autowired
    private IEmployeeService iEmployeeService;

    // Save operation
    @PostMapping("/employees")
    public Employee saveEmployee(@RequestBody Employee employees)
    {
        return iEmployeeService.saveEmployee(employees);
    }

    // Read operation
    @GetMapping("/employees")
    public List<Employee> fetchEmployeeList()
    {
        return iEmployeeService.getAllEmployee();
    }

    // Update operation
    @PutMapping("/update")
    public Employee updateDepartment(@RequestBody Employee employee)
    {
        return iEmployeeService.updateEmployee(employee);
    }

    // Delete operation
    @PostMapping("/delete")
    public String deleteEmployeeById(@RequestBody Employee employee)
    {
        iEmployeeService.deleteEmployeeById(employee);
        return "Deleted Successfully";
    }
}
