package crud.controller;

import crud.entity.Address;
import crud.entity.Employee;
import crud.service.IAddressService;
import crud.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v")
public class AddressController {

    @Autowired
    private IAddressService iAddressService;

    // Save operation
    @PostMapping("/save")
    public Address saveAddress(@RequestBody Address address)
    {
        return iAddressService.saveAddress(address);
    }

    // Read operation
    @GetMapping("/fetch")
    public List<Address> fetchAddressList()
    {
        return iAddressService.getAllAddress();
    }

    // Update operation
    @PutMapping("/update")
    public Address updateAddress(@RequestBody Address address)
    {
        return iAddressService.updateAddress(address);
    }

    // Delete operation
    @PostMapping("/delete")
    public String deleteAddressById(@RequestBody Address address)
    {
        iAddressService.deleteAddressById(address);
        return "Deleted Successfully";
    }

}
