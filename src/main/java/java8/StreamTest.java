package java8;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {

    private static String PREFIX = "{";
    private static String SUFFIX = "}";

//https://javatechonline.com/java-8-features/#How_to_create_Streams

    public static void main(String[] args) {
        //create Empty STream
      //  We create an empty Stream with the help of empty() method when we want to avoid returning null for Streams having no element. For example
        Stream<String> streamEmpty= Stream.empty();
      //  streamEmpty.forEach(System.out::println);

        //Stream of Arrays
        //One of the sources is an Array to create a stream. We call it Stream of Array. Below are the two ways to create an Array of Streams. For example:

        String[] arrayStream=new String[]{"V","A","I"};
        Stream<String> streamOfArray= Arrays.stream(arrayStream);
    //    streamOfArray.forEach(System.out::println) ;

        Stream<String> streamOfArrays=Stream.of("V","A","I");

        //Stream of Collection
        //Needless to say, we can create stream of any type of Collection like Set, List. For example:
        List<String> collection = Arrays.asList("x", "y", "z");
        Stream<String> streamOfCollection =collection.stream();
        //streamOfCollection.forEach(System.out::println) ;

       // List<String> list = List.of("x", "y", "z");
        //Stream<String> streamOfList = list.stream();

        IntStream streamOfChars = "PQR".chars();
        //streamOfChars.forEach(System.out::println);

        Stream<String> generatedStream = Stream.generate(() -> "pqr").limit(20);
        //generatedStream.forEach(System.out::println);

        Stream<String> streamBuilder = Stream.<String>builder().add("p").add("q").add("r").build();
       // streamBuilder.forEach(System.out::println);

        Stream<Integer> streamIterated = Stream.iterate(24, n -> n + 4).limit(20);
       // streamIterated.forEach(System.out::println);



        StringJoiner joiner = new StringJoiner(", ", PREFIX, SUFFIX);
        joiner.add("Core Java")
                .add("Spring Boot")
                .add("Angular");
        System.out.println(joiner.toString());

    }
}
