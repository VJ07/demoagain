package java8;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapTest {

    public static void main(String[] args) {
        List<Employee> employeeList =new ArrayList<>();
        employeeList.add(new Employee("Vaibhav", Arrays.asList(".Net","C++","Java")));
        employeeList.add(new Employee("Rahul", Arrays.asList(".Jquery","Angular","React")));
      List<String> returnString=employeeList.stream().flatMap(s->s.getProgs().stream()).sorted().collect(Collectors.toList());
      returnString.forEach(System.out::print);
    }


    public static class Employee
    {
        private String name;
        private List<String> progs;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getProgs() {
            return progs;
        }

        public void setProgs(List<String> progs) {
            this.progs = progs;
        }

        public Employee(String name, List<String> progs)
        {
            this.name=name;
            this.progs=progs;
        }

    }
}
