package demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class Root {
    @JsonProperty("Test")
   public Test test;
    @JsonProperty("Test2")
    public Test2 test2;

}

