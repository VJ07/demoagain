package demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Testing {
    /* Import Jackson libraries
Assuming that you have your favorite IDE opened, your next step is to import the Jackson packages and
create the classes returned from the tool.

You will need three Jackson packages:
jackson-core-2.11.1
jackson-databind-2.11.1
jackson-annotations-2.11.1

The "jackson-annotations-2.11.1" is used to add the "JsonProperty" attributes. The "jackson-databind-2.11.1" is
used to create the ObjectMapper class which will help us in reading the JSON and map it to our Root Object.
*/
    public static void main(String[] args) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            File file = new File("E:" + File.separator + "DemoAgain" + File.separator + "test-json.json");
            Root root = mapper.readValue(file, Root.class);
            m(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void m(Root data) {
        List<Root> roots = new ArrayList<>();
        roots.add(data);
        Map<VoucherKey, List<Root>> listMap = new HashMap<>();
        for (Root root : roots) {
            List<VoucherKey> voucherKey=splitvalue(root);
            for (VoucherKey voucherKey1:voucherKey) {
                listMap.put(voucherKey1, roots);}
        }
        listMap.forEach((key, value) -> {
            System.out.println("Key" + key + "Value" + value);
        });
    }

    public static String splitvalue(String s) {
        String delicomas = null;
        String[] spli = s.split(" ");
        boolean b = false;
        for (String sp : spli)
        {
            if (b = Utils.checkNumric(sp)) {
                delicomas = sp;
            }
        }
        return delicomas;
    }



    public static List<VoucherKey> splitvalue(Root root) {
        List<VoucherKey> voucherKey=new ArrayList<>();
        List<Root> list=new ArrayList<>();
        list.add(root);
        List<String> stringList=list.stream().flatMap(r->r.getTest().getUsers().stream().map(id->id.getId())).collect(Collectors.toList());
        for(String str:stringList)
        {
            String[] spli = str.split(" ");
            boolean b = false;
            VoucherKey voucherKeyKey=new VoucherKey();
            for (String sp : spli)
            {
                if (b = Utils.checkNumric(sp)) {
                    voucherKeyKey.setId(sp);
                   }
                if(b=Utils.checkString(sp))
                {
                    voucherKeyKey.setLocation(sp);
                }
            }
            voucherKey.add(voucherKeyKey);
        }
        return voucherKey;
    }
}
@Data
@NoArgsConstructor
@ToString
class VoucherKey
{
    private String id;
    private String location;

    public VoucherKey(String id) {
        this.id = id;
    }
    public VoucherKey(String id,String location)
    {
        this.id=id;
        this.location=location;
    }
}
