package demo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
public class Test {
    public int id;
    public String userid;
    public Object object;
    public String created_at;
    public String updated_at;
    public List<User> users;
}
