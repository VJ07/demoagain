package key;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoucherKey {


    private String vName;
    private String vId;
//
//    public VoucherKey(VoucherKey voucherKey) {
//        this.vName=voucherKey.getVName();
//        this.vId=voucherKey.getVId();
//    }
//
//    public VoucherKey(String vName,String vId) {
//        this.vName=vName;
//        this.vId=vId;
//    }

    public VoucherKey(Object o) {
    }
}
