package key;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Employee {

    private String name;
    private String rollNo;
    private String eId;
    private String state;
}
