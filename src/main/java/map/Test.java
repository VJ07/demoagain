package map;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Test {

    private Employee employee;
    private Address address;
}
