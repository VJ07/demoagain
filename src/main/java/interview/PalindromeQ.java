package interview;

public class PalindromeQ {

    public static void main(String[] args) {
        String input="I am nitin";
        String[] splt=input.split(" ");
        //System.out.println(input.length());
        //System.out.println(input.length()-1);
        for(String p:splt)
        {
            if(isPalindrome(p)) {
                System.out.println(p);
            }
        }
    }
    private static boolean isPalindrome(String input)
    {
        int p1=0;
        int p2=input.length()-1;
        while (p2>p1)
        {
           // System.out.println(input.charAt(p1) + "-" +  input.charAt(p2));
            if(input.charAt(p1)!=input.charAt(p2))
            {
              return  false;
            }
            p1++;
            p2--;
        }
        return true;
    }
}
