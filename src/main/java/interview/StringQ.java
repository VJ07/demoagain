package interview;

public class StringQ {

    public static void main(String[] args) {
//        String s1="abc";
//        String s2="abc";

       // System.out.println(s1==s2);//true s1 and s2 point to the same object or has code is same
        //System.out.println(s1.equals(s2));//true values are same
        //System.out.println(s1.hashCode()==s2.hashCode());

//        String s3=new String("abc");
//        System.out.println(s3.hashCode());
//        String s4="abc";
//        System.out.println(s4.hashCode());
//        System.out.println(s3==s4);//false //references are not pointed to the same object
//        System.out.println(s3.equals(s4));//values are same

        String s5=new String("abc");
        System.out.println(s5.hashCode());
        String s6=new String("abc");
        System.out.println(s6.hashCode());
        String s7="cba";
        System.out.println(s7.hashCode());
        System.out.println(s5==s6); //false

    }
}
