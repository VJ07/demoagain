package ApiUser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class User {

    @JsonProperty("address")
    public Address address;
    @JsonProperty("id")
    public Number id;
    @JsonProperty("email")
    public String email;
    @JsonProperty("username")
    public String username;
    @JsonProperty("password")
    public String password;
    @JsonProperty("name")
    public List<Name> name;
    @JsonProperty("phone")
    public String phone;
}
