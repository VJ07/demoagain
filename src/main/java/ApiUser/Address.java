package ApiUser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
public class Address {

    @JsonProperty("geolocation")
    public Geolocation geolocation;
    @JsonProperty("city")
    public String city;
    @JsonProperty("street")
    public String street;
    @JsonProperty("number")
    public int number;
    @JsonProperty("zipcode")
    public String zipcode;
}
