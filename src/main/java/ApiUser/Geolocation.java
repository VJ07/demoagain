package ApiUser;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Geolocation {

    @JsonProperty("lat")
    public String lat;
    @JsonProperty("lon")
    public String lon;
}
