package autowiredPrectice;

import org.springframework.stereotype.Service;

@Service
public class SedanCar implements Car {

    @Override
    public String drive() {
        return "Driving Sedan Car !";
    }
}
