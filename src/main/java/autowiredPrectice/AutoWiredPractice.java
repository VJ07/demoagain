package autowiredPrectice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoWiredPractice {

    public static void main(String[] args) {
        SpringApplication.run(AutoWiredPractice.class,args);
    }
}
