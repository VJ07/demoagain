package autowiredPrectice;


import org.springframework.stereotype.Service;

@Service
public class TeslaCar implements Car {

    @Override
    public String drive() {
        return "Driving Tesla Car !";
    }
}
