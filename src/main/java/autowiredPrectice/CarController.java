package autowiredPrectice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cars")
public class CarController {

//    @Qualifier("teslaCar")
//    @Autowired
//    private Car car;//byType

    //    @GetMapping
//    public String drive()
//    {
//        return car.drive();
//    }


//    @Autowired
//    private Car teslaCar;//byName
//    @GetMapping
//    public String drive()
//    {
//        return teslaCar.drive();
//    }

    //Using Constructor
    private final Car car;

      @Autowired
    public CarController(@Qualifier("teslaCar") Car car) {
        this.car = car;
    }

    @GetMapping
    public String drive()
    {
        return car.drive();
    }
}
