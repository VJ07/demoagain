package practise;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class Second {

    public static void main(String[] args) {
        LocalDate localDate=LocalDate.now();
        System.out.println(localDate);

        LocalDate firstFriday=LocalDate.of(localDate.getYear(),localDate.getMonth(),localDate.getDayOfMonth());
        System.out.println(firstFriday);

        LocalDate secondFriday = firstFriday.with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).
                with(TemporalAdjusters.next(DayOfWeek.FRIDAY));

        System.out.println(secondFriday);
    }
}
