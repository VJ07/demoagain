package schedular;

import crud.CrudApplication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class Schedular {

    public static void main(String[] args) {
        SpringApplication.run(CrudApplication.class,args);
       // log.info("Simple log statement with inputs {}, {} and {}", 1, 2, 3);
    }

    @Scheduled(fixedDelay = 1000)
    public void m()
    {
        System.out.println("HI");
    }
}
